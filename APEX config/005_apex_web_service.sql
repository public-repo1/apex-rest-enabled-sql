declare
  l_response clob;
begin
  apex_web_service.g_request_headers(1).name := 'Content-Type';
  apex_web_service.g_request_headers(1).value := 'application/sql';

  l_response := apex_web_service.make_rest_request (
          p_url => 'http://192.168.101.220:8080/ords/employees/_/sql'
      , p_http_method => 'POST'
      , p_body        => 'select sysdate from dual'      
      , p_username    => 'hr_emp'
      , p_password    => 'hr_emp' 
  );
  dbms_output.put_line(l_response);
end;
/	


declare
  l_response clob;
begin
  apex_web_service.g_request_headers(1).name := 'Content-Type';
  apex_web_service.g_request_headers(1).value := 'application/sql';

  l_response := apex_web_service.make_rest_request (
          p_url => 'http://192.168.101.220:8080/ords/employees/_/sql'
      , p_http_method => 'POST'
      , p_body        => q'[ 
                            insert into EMP (EMPNO,ENAME,JOB) values (1,'OFFICE HOURS 1','ORACLE');
                            insert into EMP (EMPNO,ENAME,JOB) values (2,'OFFICE HOURS 2','ORACLE');
                           ]'      
      , p_username    => 'hr_emp'
      , p_password    => 'hr_emp' 
  );
  dbms_output.put_line(l_response);
end;
/	



declare
  l_response clob;
begin
  apex_web_service.g_request_headers(1).name := 'Content-Type';
  apex_web_service.g_request_headers(1).value := 'application/sql';

  l_response := apex_web_service.make_rest_request (
          p_url => 'http://192.168.101.220:8080/ords/employees/_/sql'
      , p_http_method => 'POST'
      , p_body        => q'[
                            insert into EMP (EMPNO,ENAME,JOB) values (1,'OFFICE HOURS 1','ORACLE'); -- ok
                            insert into EMP (EMPNO,ENAME,JOB) values (3,'OFFICE HOURS 3','ORACLE'); -- error
                           ]'      
      , p_username    => 'hr_emp'
      , p_password    => 'hr_emp' 
  );
  dbms_output.put_line(l_response);
end;
/	
  


declare
  l_response clob;
begin
  apex_web_service.g_request_headers(1).name := 'Content-Type';
  apex_web_service.g_request_headers(1).value := 'application/sql';

  l_response := apex_web_service.make_rest_request (
          p_url => 'http://192.168.101.220:8080/ords/employees/_/sql'
      , p_http_method => 'POST'
      , p_body        => q'[
                            begin
                              insert into EMP (EMPNO,ENAME,JOB) values (1,'OFFICE HOURS 1','ORACLE'); -- error
                              insert into EMP (EMPNO,ENAME,JOB) values (4,'OFFICE HOURS 4','ORACLE'); -- error
                            end;
                           ]'      
      , p_username    => 'hr_emp'
      , p_password    => 'hr_emp' 
  );
  dbms_output.put_line(l_response);
end;
/	
  
