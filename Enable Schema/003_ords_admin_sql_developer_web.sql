-- Conectase como  sys as sysdba

drop user dummy_dba cascade;
/
create user dummy_dba identified by dummy_dba;
/
grant create session, create table, create procedure to dummy_dba;
/
grant dba, pdb_dba to dummy_dba;
/


-- Conectase como  dummy_dba
BEGIN
  ORDS.enable_schema(
    p_enabled             => TRUE,
    p_schema              => 'DUMMY_DBA',
    p_url_mapping_type    => 'BASE_PATH',
    p_url_mapping_pattern => 'dummy_dba',
    p_auto_rest_auth      => FALSE
  ); 
  COMMIT;
END;


-- Test   
http://192.168.101.220:8080/ords/sql-developer

